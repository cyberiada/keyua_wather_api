import argparse

# from config.fixtures import FixtureLoader
from service_api.app import app

from service_api.domain.user import create_user
from service_api.services.users import UserPermissions
from service_api.services.validation_schema import CreateUserSchema


def runserver(host=None, port=None):
    app.run(host, port)


def create_super_user(email, password):
    user_data = {
        "email": email,
        "password": password,
        "permission": UserPermissions.admin
    }
    user_data = CreateUserSchema().load(user_data)
    create_user(user_data)


parser = argparse.ArgumentParser(description="Flask RESTFull api skeleton", add_help=False)
subparsers = parser.add_subparsers(dest="command")

sparser = subparsers.add_parser(runserver.__name__, add_help=False, help="Discover all clients dbs")
sparser.add_argument("-h", "--host", dest="host", default="0.0.0.0", type=str, help="Host")
sparser.add_argument("-p", "--port", dest="port", default=5000, type=int, help="Port")

sparser = subparsers.add_parser(create_super_user.__name__, add_help=False, help="Create super user")
sparser.add_argument("-e", "--email", dest="email", type=str, help="e-mail")
sparser.add_argument("-p", "--password", dest="password", type=str, help="password")

parsed_args = parser.parse_args()


if parsed_args.command == runserver.__name__:
    runserver(parsed_args.host, parsed_args.port)

elif parsed_args.command == create_super_user.__name__:
    create_super_user(parsed_args.email, parsed_args.password)
