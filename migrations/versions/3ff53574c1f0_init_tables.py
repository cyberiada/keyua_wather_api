"""init tables

Revision ID: 3ff53574c1f0
Revises: 
Create Date: 2019-06-28 10:31:36.297155

"""
import uuid

from alembic import op
from sqlalchemy import (
    Column,
    ForeignKey,
    DateTime,
    String,
    Integer,
    Float,
    func,
)

from sqlalchemy.dialects.postgresql import UUID, ARRAY


# revision identifiers, used by Alembic.
revision = '3ff53574c1f0'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "city",
        Column("city_uuid", UUID(as_uuid=True), primary_key=True, default=uuid.uuid4),
        Column("name", String(128), unique=True),
    )
    op.create_table(
        "weather",
        Column("weather_uuid", UUID(as_uuid=True), primary_key=True, default=uuid.uuid4),
        Column("datetime", DateTime, nullable=False, server_default=func.current_timestamp()),
        Column("city_uuid", UUID(as_uuid=True), ForeignKey("city.city_uuid"), nullable=False),
        Column("temperature", Float, nullable=False)
    )
    op.create_table(
        "keyua_user",
        Column("user_uuid", UUID(as_uuid=True), primary_key=True, default=uuid.uuid4),
        Column("email", String(64), index=True, unique=True, nullable=False),
        Column("password", String(128), nullable=False),
        Column("cities_uuid", ARRAY(UUID(as_uuid=True))),
        Column("permission", Integer, nullable=False, server_default="200"),
    )
    op.create_check_constraint("constraint_cities_uuid_lenght", "keyua_user", "array_length(cities_uuid, 1) <= 5")


def downgrade():
    op.drop_table("city")
    op.drop_table("weather")
    op.drop_table("user")
    op.drop_constraint("constraint_cities_uuid_lenght", "keyua_user", type_="check")
