# keyua_weather_api

Installation
---

    # For production environment:
    # Create virtualenv
    export VENV_DIR=venv
    python3.7 -m venv "${VENV_DIR}"
    "${VENV_DIR}"/bin/python3.7 -m ensurepip --upgrade
    "${VENV_DIR}"/bin/python3.7 -m pip install -r requirements/requirements.txt

    # For development environment:
    make setup

    # Create superuser PostgreSQL for development
    CREATE ROLE keyua WITH SUPERUSER CREATEDB CREATEROLE LOGIN ENCRYPTED PASSWORD 'keyua';
    
    # Create database
    CREATE DATABASE keyua_weather;


Environment variables
---
These environment variables will be defined in any runtime environment and could be used in program code:

    DB_HOST              # DB listen host,      default=localhost 
    DB_PORT              # DB listen port,      default=5432
    DB_NAME              # DB Name,             default=keyua_weather 
    DB_USER              # DB user name,        default=keyua
    DB_PASSWORD          # DB password,         default=keyua


Run tests
---
    # Run flake8:
    make flake

    # Run tests:
    make test

Start app
---
    # Run migrations
    $ alembic upgrade head

    # Create admin
    python manage.py create_super_user -e admin@admin.com -p mypass

    # Run server
    python manage.py runserver  # optional key: -h 0.0.0.0 -p 5000


First steps
---
See a Postman collection in a config dir. First of all you need get a token (token will be set as env var automatically and send 
in all requests)
    