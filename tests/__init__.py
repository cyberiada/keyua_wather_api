import os
from unittest import TestCase

import pytest
from flask_jwt_extended import create_access_token

from config import FixtureLoader
from service_api.app import app
from service_api.constants import SERVICE_NAME
from service_api.models import models
from service_api.services.database import DatabaseRegistry

_here = os.path.dirname(__file__)
TEST_DB_NAME = "test_keyua"


@pytest.mark.usefixtures("db_fixture")
@pytest.mark.usefixtures("get_engine")
class BaseTestCase(TestCase):
    fixture_file = "test_data.yaml"

    base_url = f"/{SERVICE_NAME}/v1"
    headers = {"Content-Type": "application/json"}

    @property
    def test_client(self):
        return app.test_client()

    def setUp(self):
        load_fixture(self.fixture_file)

    def tearDown(self):
        clear_tables()

    def set_token(self, email):
        with app.app_context():
            access_token = create_access_token(identity=email)
            self.headers.update({"Authorization": f"Bearer {access_token}"})

    def clear_token(self):
        if "Authorization" in self.headers:
            del self.headers["Authorization"]


def clear_tables():
    db_engine = DatabaseRegistry.get()
    with db_engine.connect() as conn:
        for model in models:
            conn.execute(f"TRUNCATE TABLE {model.name} CASCADE;")


def load_fixture(fixture):
    FixtureLoader.fixture_folder = os.path.join(_here, "fixtures")
    FixtureLoader.fixture = fixture
    fixture_loader = FixtureLoader(TEST_DB_NAME)
    fixture_loader.upload()
