import json
from http import HTTPStatus

from tests import BaseTestCase


class TestUsersResource(BaseTestCase):

    def test_get_without_token(self):
        self.clear_token()

        response = self.test_client.get(f"{self.base_url}/users", headers=self.headers)
        self.assertEqual(response.status_code, HTTPStatus.UNAUTHORIZED)

    def test_get_with_common_user(self):
        self.set_token(email="test1@test.com")

        response = self.test_client.get(f"{self.base_url}/users", headers=self.headers)
        self.assertEqual(response.status_code, HTTPStatus.FORBIDDEN)

    def test_get_with_admin(self):
        self.set_token(email="admin1@test.com")

        response = self.test_client.get(f"{self.base_url}/users", headers=self.headers)
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_post_without_token(self):
        self.clear_token()
        payload = {"email": "test666@test.com", "password": "test"}

        response = self.test_client.post(f"{self.base_url}/users", data=payload, headers=self.headers)
        self.assertEqual(response.status_code, HTTPStatus.UNAUTHORIZED)

    def test_post_with_common_user(self):
        self.set_token(email="test1@test.com")
        payload = {"email": "test666@test.com", "password": "test"}

        response = self.test_client.post(f"{self.base_url}/users", data=payload, headers=self.headers)
        self.assertEqual(response.status_code, HTTPStatus.FORBIDDEN)

    def test_post_with_admin(self):
        self.set_token(email="admin1@test.com")
        payload = json.dumps({"email": "test666@test.com", "password": "test"})

        response = self.test_client.post(f"{self.base_url}/users", data=payload, headers=self.headers)
        self.assertEqual(response.status_code, HTTPStatus.CREATED)

        created_user_uuid = response.json["user_uuid"]
        response = self.test_client.get(f"{self.base_url}/users/{created_user_uuid}", headers=self.headers)
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertEqual(created_user_uuid, response.json["user_uuid"])


class TestUserResource(BaseTestCase):

    def test_get_without_token(self):
        self.clear_token()
        user_uuid = "754d15a3-abf2-4940-bc38-369b8bcfac48"

        response = self.test_client.get(f"{self.base_url}/users/{user_uuid}", headers=self.headers)
        self.assertEqual(response.status_code, HTTPStatus.UNAUTHORIZED)

    def test_get_with_common_user(self):
        self.set_token(email="test1@test.com")
        user_uuid = "754d15a3-abf2-4940-bc38-369b8bcfac48"

        response = self.test_client.get(f"{self.base_url}/users/{user_uuid}", headers=self.headers)
        self.assertEqual(response.status_code, HTTPStatus.FORBIDDEN)

    def test_get_with_admin(self):
        self.set_token(email="admin1@test.com")
        user_uuid = "754d15a3-abf2-4940-bc38-369b8bcfac48"

        response = self.test_client.get(f"{self.base_url}/users/{user_uuid}", headers=self.headers)
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_get_with_admin_invalid_user(self):
        self.set_token(email="admin1@test.com")
        user_uuid = "13bcd9e0-e05d-4e40-af7c-ac19364a82d6"

        response = self.test_client.get(f"{self.base_url}/users/{user_uuid}", headers=self.headers)
        self.assertEqual(response.status_code, HTTPStatus.NOT_FOUND)

    def test_delete_without_token(self):
        self.clear_token()
        user_uuid = "754d15a3-abf2-4940-bc38-369b8bcfac48"

        response = self.test_client.delete(f"{self.base_url}/users/{user_uuid}", headers=self.headers)
        self.assertEqual(response.status_code, HTTPStatus.UNAUTHORIZED)

    def test_delete_with_common_user(self):
        self.set_token(email="test1@test.com")
        user_uuid = "754d15a3-abf2-4940-bc38-369b8bcfac48"

        response = self.test_client.get(f"{self.base_url}/users/{user_uuid}", headers=self.headers)
        self.assertEqual(response.status_code, HTTPStatus.FORBIDDEN)

    def test_delete_with_admin(self):
        self.set_token(email="admin1@test.com")
        user_uuid = "754d15a3-abf2-4940-bc38-369b8bcfac48"

        response = self.test_client.get(f"{self.base_url}/users/{user_uuid}", headers=self.headers)
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertEqual(user_uuid, response.json["user_uuid"])

        response = self.test_client.delete(f"{self.base_url}/users/{user_uuid}", headers=self.headers)
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertEqual(user_uuid, response.json["user_uuid"])

        response = self.test_client.get(f"{self.base_url}/users/{user_uuid}", headers=self.headers)
        self.assertEqual(response.status_code, HTTPStatus.NOT_FOUND)

    def test_put_without_token(self):
        self.clear_token()
        user_uuid = "754d15a3-abf2-4940-bc38-369b8bcfac48"
        payload = {"email": "test666@test.com", "permission": 200}

        response = self.test_client.put(f"{self.base_url}/users/{user_uuid}", data=payload, headers=self.headers)
        self.assertEqual(response.status_code, HTTPStatus.UNAUTHORIZED)

    def test_put_with_common_user(self):
        self.set_token(email="test1@test.com")
        user_uuid = "754d15a3-abf2-4940-bc38-369b8bcfac48"
        payload = {"email": "test666@test.com", "permission": 200}

        response = self.test_client.put(f"{self.base_url}/users/{user_uuid}", data=payload, headers=self.headers)
        self.assertEqual(response.status_code, HTTPStatus.FORBIDDEN)

    def test_put_with_admin(self):
        self.set_token(email="admin1@test.com")
        user_uuid = "754d15a3-abf2-4940-bc38-369b8bcfac48"
        payload = {"email": "test666@test.com", "permission": 1000, "cities_uuid": ["154a9043-bd3d-4fde-9c58-a6cb66e80e2e"]}
        json_payload = json.dumps(payload)

        response = self.test_client.get(f"{self.base_url}/users/{user_uuid}", headers=self.headers)
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertEqual(user_uuid, response.json["user_uuid"])
        self.assertEqual("test1@test.com", response.json["email"])
        self.assertEqual(200, response.json["permission"])
        self.assertEqual(["885f81c6-b2bc-4759-9617-6bf6b3d069fb"], response.json["cities_uuid"])

        response = self.test_client.put(f"{self.base_url}/users/{user_uuid}", data=json_payload, headers=self.headers)
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertEqual(user_uuid, response.json["user_uuid"])
        self.assertEqual(payload["email"], response.json["email"])
        self.assertEqual(payload["permission"], response.json["permission"])
        self.assertEqual(payload["cities_uuid"], response.json["cities_uuid"])

    def test_put_with_admin_invalid_user(self):
        user_uuid = "13bcd9e0-e05d-4e40-af7c-ac19364a82d6"
        payload = {"email": "test666@test.com", "permission": 1000}

        response = self.test_client.get(f"{self.base_url}/users/{user_uuid}", data=payload, headers=self.headers)
        self.assertEqual(response.status_code, HTTPStatus.NOT_FOUND)

    def test_subscribe_user_more_than_five_cities(self):
        self.set_token(email="admin1@test.com")
        user_uuid = "754d15a3-abf2-4940-bc38-369b8bcfac48"
        cities_uuid = [
                "885f81c6-b2bc-4759-9617-6bf6b3d069fb",
                "154a9043-bd3d-4fde-9c58-a6cb66e80e2e",
                "6bee691d-1ed4-4dd3-9a67-70adc852c93d",
                "d6e89a7a-c732-4ef9-8786-2d0b8f3db867",
                "0826dd88-1914-4e33-a916-5bd5af824cff",
                "7a8ed54d-ce7b-4ef5-9917-c16d96620d53"
        ]

        payload = {
            "email": "test666@test.com",
            "permission": 1000,
            "cities_uuid": cities_uuid
        }
        json_payload = json.dumps(payload)

        response = self.test_client.put(f"{self.base_url}/users/{user_uuid}", data=json_payload, headers=self.headers)
        self.assertEqual(response.status_code, HTTPStatus.UNPROCESSABLE_ENTITY)
