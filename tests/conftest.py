import pytest

from service_api.models import metadata
from service_api.services.database import DatabaseRegistry
from tests import TEST_DB_NAME


test_db_engine = DatabaseRegistry.get(TEST_DB_NAME)


@pytest.fixture(scope="session", autouse=True)
def db_fixture():
    db_engine = DatabaseRegistry.get()

    db_exists_query = f"SELECT datname FROM pg_catalog.pg_database WHERE datname='{TEST_DB_NAME}';"
    create_db_query = f"CREATE DATABASE {TEST_DB_NAME};"
    terminate_query = f"SELECT pg_terminate_backend(pid) FROM pg_stat_activity where datname = '{TEST_DB_NAME}';"
    drop_db_query = f"DROP DATABASE IF EXISTS {TEST_DB_NAME};"

    with db_engine.connect() as conn:
        cur = conn.execute(db_exists_query)
        test_db_exists = cur.fetchone()
        if test_db_exists:
            conn.execute(terminate_query)
            conn.execute("commit")
            conn.execute(drop_db_query)

        conn.execute("commit")
        conn.execute(create_db_query)

    metadata.create_all(test_db_engine)

    yield

    with db_engine.connect() as conn:
        conn.execute(terminate_query)
        conn.execute("commit")
        conn.execute(drop_db_query)


@pytest.fixture
def get_engine(monkeypatch):
    def mock(*args, **kwargs):
        return test_db_engine
    return monkeypatch.setattr(DatabaseRegistry, "get", mock)
