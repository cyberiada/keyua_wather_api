from sqlalchemy import select, insert, delete, update
from sqlalchemy.exc import IntegrityError
from werkzeug.exceptions import NotFound, UnprocessableEntity

from service_api.models import City
from service_api.services.database import db_execute_many, db_execute_one, DatabaseRegistry


def create_city(city_data):
    db_engine = DatabaseRegistry.get()
    stmt = insert(City).returning(City).values(**city_data)

    try:
        created_city = db_execute_one(db_engine, stmt)
    except IntegrityError:
        raise UnprocessableEntity(f"City '{city_data['name']}' already exists")

    return dict(created_city)


def get_cities():
    db_engine = DatabaseRegistry.get()
    stmt = select([City.c.city_uuid, City.c.name])
    cities = db_execute_many(db_engine, stmt)

    if not cities:
        raise NotFound("Cities not found")

    return [dict(city) for city in cities]


def get_city(city_uuid):
    db_engine = DatabaseRegistry.get()
    stmt = select([City.c.city_uuid, City.c.name]).where(City.c.city_uuid == city_uuid)
    city = db_execute_one(db_engine, stmt)

    if not city:
        raise NotFound(f"City '{city_uuid}' not found")

    return dict(city)


def update_city(user_data, city_uuid):
    db_engine = DatabaseRegistry.get()
    stmt = update(City).where(City.c.city_uuid == city_uuid).values(**user_data).returning(
        City.c.city_uuid,
        City.c.name,
    )
    updated_user = db_execute_one(db_engine, stmt)

    if not updated_user:
        raise NotFound(f"City '{city_uuid}' for update not found")

    return dict(updated_user)


def delete_city(city_uuid):
    db_engine = DatabaseRegistry.get()
    stmt = delete(City).where(City.c.city_uuid == city_uuid).returning(City)
    deleted_city = db_execute_one(db_engine, stmt)

    if not deleted_city:
        raise NotFound(f"City '{city_uuid}' for delete not found")

    return dict(deleted_city)
