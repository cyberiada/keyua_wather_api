from sqlalchemy import select, insert, delete, update
from sqlalchemy.exc import IntegrityError
from werkzeug.exceptions import NotFound, UnprocessableEntity

from service_api.models import Weather
from service_api.services.database import db_execute_many, db_execute_one, DatabaseRegistry


def create_weather(city_uuid, weather_data):
    db_engine = DatabaseRegistry.get()

    created_weather = []

    for weather in weather_data:

        insert_data = dict(weather)
        insert_data["city_uuid"] = city_uuid
        stmt = insert(Weather).returning(Weather).values(**insert_data)

        try:
            result = db_execute_one(db_engine, stmt)
        except IntegrityError:
            raise UnprocessableEntity(f"Weather '{weather['weather_uuid']}' already exists")

        created_weather.append(dict(result))

    return created_weather


def get_weather_city(city_uuid):
    db_engine = DatabaseRegistry.get()
    stmt = select([Weather]).where(Weather.c.city_uuid == city_uuid)
    weather = db_execute_many(db_engine, stmt)

    if not weather:
        raise NotFound(f"Weather '{city_uuid}' not found")

    return [dict(w) for w in weather]


def get_weather(weather_uuid):
    db_engine = DatabaseRegistry.get()
    stmt = select([Weather]).where(Weather.c.weather_uuid == weather_uuid)
    weather = db_execute_one(db_engine, stmt)

    if not weather:
        raise NotFound(f"Weather '{weather_uuid}' not found")

    return dict(weather)


def update_weather(weather_uuid, weather_data):
    db_engine = DatabaseRegistry.get()
    stmt = update(Weather).where(Weather.c.weather_uuid == weather_uuid).values(**weather_data).returning(Weather)
    updated_user = db_execute_one(db_engine, stmt)

    if not updated_user:
        raise NotFound(f"Weather '{weather_uuid}' for update not found")

    return dict(updated_user)


def delete_weather(weather_uuid):
    db_engine = DatabaseRegistry.get()
    stmt = delete(Weather).where(Weather.c.weather_uuid == weather_uuid).returning(Weather)
    deleted_weather = db_execute_one(db_engine, stmt)

    if not deleted_weather:
        raise NotFound(f"Weather '{weather_uuid}' for delete not found")

    return dict(deleted_weather)
