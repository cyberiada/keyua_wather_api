import bcrypt
from flask_jwt_extended import create_access_token
from sqlalchemy.exc import IntegrityError
from werkzeug.exceptions import UnprocessableEntity, NotFound
from sqlalchemy import select, update, delete
from sqlalchemy.dialects.postgresql import insert

from service_api.models import KeyuaUser
from service_api.services.database import db_execute_many, db_execute_one, DatabaseRegistry


def create_user(user_data):
    db_engine = DatabaseRegistry.get()
    stmt = insert(KeyuaUser).returning(
        KeyuaUser.c.user_uuid,
        KeyuaUser.c.email,
        KeyuaUser.c.cities_uuid
    ).values(**user_data)

    try:
        created_user = db_execute_one(db_engine, stmt)
    except IntegrityError:
        raise UnprocessableEntity(f"User '{user_data['email']}' already exists")

    return dict(created_user)


def get_users():
    db_engine = DatabaseRegistry.get()
    stmt = select([
        KeyuaUser.c.user_uuid,
        KeyuaUser.c.email,
        KeyuaUser.c.cities_uuid,
        KeyuaUser.c.permission
    ])
    users = db_execute_many(db_engine, stmt)

    if not users:
        raise NotFound("Users not found")

    return [dict(user) for user in users]


def get_user(user_uuid):
    db_engine = DatabaseRegistry.get()
    stmt = select([
        KeyuaUser.c.user_uuid,
        KeyuaUser.c.email,
        KeyuaUser.c.cities_uuid,
        KeyuaUser.c.permission
    ]).where(
        KeyuaUser.c.user_uuid == user_uuid
    )
    user = db_execute_one(db_engine, stmt)

    if not user:
        raise NotFound(f"User '{user_uuid}' not found")

    return dict(user)


def _get_user_by_email(email):
    db_engine = DatabaseRegistry.get()
    stmt = select([KeyuaUser]).where(KeyuaUser.c.email == email)
    user = db_execute_one(db_engine, stmt)

    if not user:
        raise NotFound(f"User '{email}' not found")

    return dict(user)


def update_user(user_data, user_uuid):
    db_engine = DatabaseRegistry.get()
    stmt = update(KeyuaUser).where(KeyuaUser.c.user_uuid == user_uuid).values(**user_data).returning(
        KeyuaUser.c.user_uuid,
        KeyuaUser.c.email,
        KeyuaUser.c.cities_uuid,
        KeyuaUser.c.permission
    )
    try:
        updated_user = db_execute_one(db_engine, stmt)
    except IntegrityError:
        cities_uuid = user_data["cities_uuid"]
        raise UnprocessableEntity(f"User can't subscribe more then 5 cities. \n {cities_uuid}")

    if not updated_user:
        raise NotFound(f"User '{user_uuid}' for update not found")

    return dict(updated_user)


def delete_user(user_uuid):
    db_engine = DatabaseRegistry.get()
    stmt = delete(KeyuaUser).where(KeyuaUser.c.user_uuid == user_uuid).returning(
        KeyuaUser.c.user_uuid,
        KeyuaUser.c.email,
        KeyuaUser.c.cities_uuid,
        KeyuaUser.c.permission
    )
    deleted_user = db_execute_one(db_engine, stmt)

    if not deleted_user:
        raise NotFound(f"User '{user_uuid}' for delete not found")

    return dict(deleted_user)


def get_token(auth):
    user = _get_user_by_email(auth.username)

    if not auth and not check_password(auth.password, user["password"]):
        raise UnprocessableEntity("Incorrect user login or password")

    access_token = create_access_token(identity=user["email"])
    return access_token


def get_hashed_password(plain_text_password):
    return bcrypt.hashpw(plain_text_password, bcrypt.gensalt())


def check_password(plain_text_password, hashed_password):
    return bcrypt.checkpw(plain_text_password, hashed_password)
