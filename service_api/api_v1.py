from flask_restful import Api

from service_api.constants import SERVICE_NAME


def load_api(app):

    from service_api.resources import SmokeResource
    from service_api.resources.user import UsersResource, UserResource, UserLogin
    from service_api.resources.city import CitiesResource, CityResource
    from service_api.resources.weather import WeatherCityResource, WeatherResource

    api_v1 = Api(app, f"/{SERVICE_NAME}/v1")

    api_v1.add_resource(SmokeResource, "/smoke")

    api_v1.add_resource(UsersResource, "/users")
    api_v1.add_resource(UserResource, "/users/<user_uuid>")
    api_v1.add_resource(UserLogin, "/users/login")

    api_v1.add_resource(CitiesResource, "/cities")
    api_v1.add_resource(CityResource, "/cities/<city_uuid>")

    api_v1.add_resource(WeatherCityResource, "/cities/<city_uuid>/weather")
    api_v1.add_resource(WeatherResource, "/weather/<weather_uuid>")
