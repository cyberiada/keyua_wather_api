from http import HTTPStatus

from flask import request

from service_api.domain.weather import create_weather, get_weather_city, update_weather, delete_weather, get_weather
from service_api.resources import BaseResource
from service_api.services.decorators import authorized
from service_api.services.response import json_response
from service_api.services.users import UserPermissions
from service_api.services.validation_schema import CreateWeatherSchema


class WeatherCityResource(BaseResource):

    @authorized(required_permission=UserPermissions.admin)
    def post(self, city_uuid):
        request_data = CreateWeatherSchema(many=True).load(request.json)
        created_weather = create_weather(city_uuid, request_data)
        return json_response(created_weather), HTTPStatus.CREATED

    @authorized(required_permission=UserPermissions.common)
    def get(self, city_uuid):
        weather = get_weather_city(city_uuid)
        return json_response(data=weather)


class WeatherResource(BaseResource):

    @authorized(required_permission=UserPermissions.common)
    def get(self, weather_uuid):
        weather = get_weather(weather_uuid)
        return json_response(data=weather)

    @authorized(required_permission=UserPermissions.admin)
    def put(self, weather_uuid):
        request_data = CreateWeatherSchema().load(request.json)
        updated_weather = update_weather(weather_uuid, request_data)
        return json_response(data=updated_weather)

    @authorized(required_permission=UserPermissions.admin)
    def delete(self, weather_uuid):
        updated_weather = delete_weather(weather_uuid)
        return json_response(data=updated_weather)
