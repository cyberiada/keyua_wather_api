from http import HTTPStatus

from flask import request, jsonify

from service_api.resources import BaseResource
from service_api.services.decorators import authorized
from service_api.services.response import json_response
from service_api.services.users import UserPermissions
from service_api.services.validation_schema import CreateUserSchema, UpdateUserSchema
from service_api.domain.user import create_user, get_users, update_user, get_user, delete_user, get_token


class UsersResource(BaseResource):

    @authorized(required_permission=UserPermissions.admin)
    def post(self):
        request_data = CreateUserSchema().load(request.json)
        created_user = create_user(request_data)
        return json_response(created_user), HTTPStatus.CREATED

    @authorized(required_permission=UserPermissions.admin)
    def get(self):
        users = get_users()
        return json_response(data=users)


class UserResource(BaseResource):

    @authorized(required_permission=UserPermissions.admin)
    def get(self, user_uuid):
        users = get_user(user_uuid)
        return json_response(data=users)

    @authorized(required_permission=UserPermissions.admin)
    def put(self, user_uuid):
        request_data = UpdateUserSchema().load(request.json)
        updated_user = update_user(request_data, user_uuid)
        return json_response(updated_user), HTTPStatus.OK

    @authorized(required_permission=UserPermissions.admin)
    def delete(self, user_uuid):
        deleted_user = delete_user(user_uuid)
        return json_response(deleted_user), HTTPStatus.OK


class UserLogin(BaseResource):
    method_decorators = []

    def post(self):
        auth = request.authorization
        token = get_token(auth)
        return jsonify({"token": token})
