from http import HTTPStatus

from flask import request

from service_api.resources import BaseResource
from service_api.services.decorators import authorized
from service_api.services.response import json_response
from service_api.services.users import UserPermissions
from service_api.services.validation_schema import CreateCitySchema
from service_api.domain.city import get_cities, create_city, get_city, delete_city, update_city


class CitiesResource(BaseResource):

    @authorized(required_permission=UserPermissions.admin)
    def post(self):
        request_data = CreateCitySchema().load(request.json)
        created_city = create_city(request_data)
        return json_response(created_city), HTTPStatus.CREATED

    @authorized(required_permission=UserPermissions.common)
    def get(self):
        users = get_cities()
        return json_response(data=users)


class CityResource(BaseResource):

    @authorized(required_permission=UserPermissions.common)
    def get(self, city_uuid):
        city = get_city(city_uuid)
        return json_response(data=city)

    @authorized(required_permission=UserPermissions.admin)
    def put(self, city_uuid):
        request_data = CreateCitySchema().load(request.json)
        updated_city = update_city(request_data, city_uuid)
        return json_response(updated_city)

    @authorized(required_permission=UserPermissions.admin)
    def delete(self, city_uuid):
        deleted_user = delete_city(city_uuid)
        return json_response(deleted_user)
