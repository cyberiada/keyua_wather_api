from flask_restful import Resource


class BaseResource(Resource):
    pass


class SmokeResource(BaseResource):
    def get(self):
        return {"hello": "world"}
