from marshmallow import Schema, fields, post_load

from service_api.services.users import UserPermissions
from service_api.domain.user import get_hashed_password


class BaseSchema(Schema):
    pass


class CreateUserSchema(BaseSchema):
    email = fields.Email(required=True)
    password = fields.String(required=True)
    permission = fields.Integer(missing=UserPermissions.common)

    @post_load
    def hash_pass(self, data, **kwargs):
        data["password"] = get_hashed_password(data["password"])
        return data


class UpdateUserSchema(BaseSchema):
    email = fields.Email(required=True)
    password = fields.String()
    permission = fields.Integer()
    cities_uuid = fields.List(fields.String(), required=True)

    @post_load
    def hash_pass(self, data, **kwargs):
        if "password" in data:
            data["password"] = get_hashed_password(data["password"])
        return data


class CreateCitySchema(BaseSchema):
    name = fields.String(required=True)


class CreateWeatherSchema(BaseSchema):
    temperature = fields.Float(required=True)
    datetime = fields.DateTime()
