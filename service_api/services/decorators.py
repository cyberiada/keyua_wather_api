from flask_jwt_extended import get_jwt_identity, jwt_required
from werkzeug.exceptions import Forbidden
from service_api.domain.user import _get_user_by_email


def authorized(required_permission):
    def decorator(func):
        @jwt_required
        def wrapper(*args, **kwargs):
            user_email = get_jwt_identity()

            user = _get_user_by_email(user_email)
            user_permission = user["permission"]

            if user_permission < required_permission:
                raise Forbidden("User doesn't have enough permissions")

            return func(*args, **kwargs)

        return wrapper
    return decorator
