import datetime
import uuid

from sqlalchemy import (
    Table,
    MetaData,
    Column,
    ForeignKey,
    DateTime,
    String,
    Integer,
    Float,
    CheckConstraint
)
from sqlalchemy.dialects.postgresql import UUID, ARRAY

from service_api.services.users import UserPermissions

metadata = MetaData()


City = Table(
    "city",
    metadata,
    Column("city_uuid", UUID(as_uuid=True), primary_key=True, default=uuid.uuid4),
    Column("name", String(128), index=True, unique=True),
)


Weather = Table(
    "weather",
    metadata,
    Column("weather_uuid", UUID(as_uuid=True), primary_key=True, default=uuid.uuid4),
    Column("datetime", DateTime, nullable=False, default=datetime.datetime.utcnow()),
    Column("city_uuid", UUID(as_uuid=True), ForeignKey("city.city_uuid"), nullable=False),
    Column("temperature", Float, nullable=False)
)


KeyuaUser = Table(
    "keyua_user",
    metadata,
    Column("user_uuid", UUID(as_uuid=True), primary_key=True, default=uuid.uuid4),
    Column("email", String(64), index=True, unique=True, nullable=False),
    Column("password", String(128), nullable=False),
    Column("cities_uuid", ARRAY(String()), CheckConstraint("array_length(cities_uuid, 1) <= 5")),
    Column("permission", Integer, nullable=False, default=UserPermissions.common),
)


models = (City, Weather, KeyuaUser)
