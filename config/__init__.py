import os

import yaml

from service_api.models import models
from service_api.services.database import DatabaseRegistry


_here = os.path.dirname(__name__)


class FixtureLoader:

    fixture_folder = os.path.join(_here, "fixtures")
    fixture = "init.yaml"

    def __init__(self, db_name=None):
        self.db_name = db_name
        self.db_engine = DatabaseRegistry.get(db_name)

    @property
    def fixture_path(self):
        fpath = os.path.abspath(os.path.join(self.fixture_folder, self.fixture))
        return fpath

    def _load_data_from_yaml(self):
        with open(self.fixture_path) as fp:
            return yaml.load(fp, Loader=yaml.FullLoader)

    def upload(self):
        data = self._load_data_from_yaml()

        with self.db_engine.begin() as conn:
            for model in models:
                records = data.get(model.name, [])
                self._upload_records(model, conn, records)

    def _upload_records(self, table, conn, records):
        for record in records:
            conn.execute(table.insert().values(**record))
